package com.changgou.framework.exception;

import com.changgou.util.Result;
import com.changgou.util.StatusCode;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Jun
 * @date 2021/1/8
 */
@ControllerAdvice
public class BaseException {

    /**
     * 统一错误处理返回信息
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result baseError(Exception e) {
        e.printStackTrace();
        //返回统一的错误信息
        return new Result(false, StatusCode.ERROR, "系统繁忙，请稍后再试！");
    }
}
