package com.changgou.canal.listener;

import com.alibaba.fastjson.JSON;
import com.alibaba.otter.canal.protocol.CanalEntry;
import com.changgou.content.feign.ContentFeign;
import com.changgou.content.pojo.Content;
import com.changgou.util.Result;
import com.xpand.starter.canal.annotation.CanalEventListener;
import com.xpand.starter.canal.annotation.ListenPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

/**
 * @author Jun
 * @date 2021/1/10
 */
@CanalEventListener
public class CanalDataEventListener {
    @Autowired
    private ContentFeign contentFeign;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 修改广告数据监听
     * 监听广告的增删改
     * @param eventType
     * @param rowData
     */
    @ListenPoint(destination = "example",
            schema = "changgou_content",
            table = {"tb_content"},
            eventType = {CanalEntry.EventType.UPDATE,
                         CanalEntry.EventType.INSERT,
                         CanalEntry.EventType.DELETE
            })
    public void onEventCustomUpdate(CanalEntry.EventType eventType, CanalEntry.RowData rowData) {
        //获取广告分类id
        String categoryId = getColumn(rowData, "category_id");
        Result<List<Content>> all = contentFeign.findAll();
        System.out.println(all);
        //根据广告分类id获取其下的所有广告信息
        //Result<List<Content>> result = contentFeign.findByCategoryId(Long.valueOf(categoryId));
        //将数据广告存入到
        //List<Content> contentList = result.getData();
        //stringRedisTemplate.boundValueOps("content_" + categoryId).set(JSON.toJSONString(contentList));

    }


    /**
     * 获取指定的值
     * @param rowData
     * @param columnName
     * @return
     */
    private String getColumn(CanalEntry.RowData rowData, String columnName) {
        //获取修改列的值
        for (CanalEntry.Column column : rowData.getAfterColumnsList()) {
            if (column.getName().equals(columnName)) {
                return column.getValue();
            }
        }
        //删除操作也要获取
        for (CanalEntry.Column column : rowData.getBeforeColumnsList()) {
            if (column.getName().equals(columnName)) {
                return column.getValue();
            }
        }

        return null;
    }
}
