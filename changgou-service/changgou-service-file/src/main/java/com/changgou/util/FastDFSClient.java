package com.changgou.util;

import com.changgou.file.FastDFSFile;
import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.springframework.core.io.ClassPathResource;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Jun
 * @date 2021/1/8
 */
public class FastDFSClient {

    /***
     * 初始化tracker信息
     */
    static {
        try {
            //获取tracker的配置文件fdfs_client.conf的位置
            String filePath = new ClassPathResource("fdfs_client.conf").getPath();
            //加载tracker配置信息
            ClientGlobal.init(filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 上传文件
     * @param file
     * @return
     */
    public static String[] upload(FastDFSFile file) {
        //文件附加参数
        NameValuePair[] meta_list = new NameValuePair[1];
        meta_list[0] =new NameValuePair(file.getAuthor());

        String[] uploadResults = null;
        //获取操作客户端
        StorageClient storageClient = createStorageClient();
        try {
            uploadResults = storageClient.upload_file(file.getContent(), file.getExt(), meta_list);
        } catch (Exception e) {
            e.printStackTrace();
            //上传失败
            return null;
        }
        //返回上传成功信息
        // uploadResults[0]:文件上传所存储的组名，例如:group1
        // uploadResults[1]:文件存储路径,例如：M00/00/00/wKjThF0DBzaAP23MAAXz2mMp9oM26.jpeg
        return uploadResults;
    }

    /**
     * 获取文件信息
     * @param groupName
     * @param remoteFileName
     * @return
     */
    public static FileInfo getFileInfo(String groupName, String remoteFileName) {
        FileInfo file_info = null;
        try {
            //获取文件操作客户端对象
            StorageClient storageClient = createStorageClient();
            //获取文件信息
            file_info = storageClient.get_file_info(groupName, remoteFileName);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        //返回文件信息
        return file_info;
    }


    /**
     * 下载文件
     * @param groupName
     * @param remoteFileName
     * @return
     */
    public static InputStream downLoadFile(String groupName, String remoteFileName) {
        try {
            //获取文件操作客户端对象
            StorageClient storageClient = createStorageClient();
            //下载文件
            byte[] file = storageClient.download_file(groupName, remoteFileName);
            //讲文件转化为字节输入流
            return new ByteArrayInputStream(file);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 删除文件
     * @param groupName
     * @param remoteFileName
     */
    public static void deleteFile(String groupName, String remoteFileName) {
        try {
            //获取文件操作客户端对象
            StorageClient storageClient = createStorageClient();
            //删除文件
            storageClient.delete_file(groupName, remoteFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取文件操作客户端
     * @return
     */
    private static StorageClient createStorageClient() {
        //文件操作客户端
        StorageClient storageClient = null;
        try {
            //创建TrackerClient客户端对象
            TrackerClient trackerClient = new TrackerClient();
            //通过TrackerClient对象获取TrackerServer信息
            TrackerServer trackerServer = trackerClient.getConnection();
            //获取StorageClient对象
            storageClient = new StorageClient(trackerServer, null);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return storageClient;
    }

}
