package com.changgou.controller;

import com.changgou.file.FastDFSFile;
import com.changgou.util.FastDFSClient;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author Jun
 * @date 2021/1/9
 */
@RestController
@CrossOrigin
public class FileController {

    /**
     * 上传文件
     * @param file
     * @return
     */
    @PostMapping(value = "/upload")
    public String upload(@RequestParam(value = "file") MultipartFile file) throws IOException {
        //封装上传文件数据
        FastDFSFile fastDFSFile = new FastDFSFile();
        //文件名
        fastDFSFile.setName(file.getOriginalFilename());
        //参数名暂时作为作者名传入
        fastDFSFile.setAuthor(file.getName());
        //文件内容
        fastDFSFile.setContent(file.getBytes());
        //文件扩展名
        fastDFSFile.setExt(StringUtils.getFilenameExtension(file.getOriginalFilename()));

        //执行文件上传
        String[] upload = FastDFSClient.upload(fastDFSFile);
        //确保文件上传成功后返回
        if (upload != null && upload.length > 0) {
            //文件上传后查询地址
            return "images-changgou-java.itheima.net/" + upload[0] + "/" + upload[1];
        }
        //上传失败
        return null;
    }

}
