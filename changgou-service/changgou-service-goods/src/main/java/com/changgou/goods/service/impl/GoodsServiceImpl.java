package com.changgou.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.changgou.goods.dao.BrandMapper;
import com.changgou.goods.dao.CategoryMapper;
import com.changgou.goods.dao.SkuMapper;
import com.changgou.goods.dao.SpuMapper;
import com.changgou.goods.pojo.*;
import com.changgou.goods.service.GoodsService;
import com.changgou.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Jun
 * @date 2021/1/10
 */
@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private SpuMapper spuMapper;

    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private BrandMapper brandMapper;

    @Autowired
    private IdWorker idWorker;
    //已被删除状态
    final static String ISDELETE = "1";
    //上架状态
    final static String ISMARKET = "1";


    /**
     * 物理删除商品
     *
     * @param spuId
     */
    @Override
    public void delete(String spuId) {
        //查询商品信息
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        if (!ISDELETE.equals(spu.getIsDelete())) {
            throw new RuntimeException("未被逻辑删除的商品不能被物理删除！");
        }
        spuMapper.deleteByPrimaryKey(spuId);
    }

    /**
     * 恢复逻辑删除商品
     *
     * @param spuId
     */
    @Override
    public void restore(String spuId) {
        //查询商品信息
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        //检查是否为已删除数据
        if (!ISDELETE.equals(spu.getIsDelete())) {
            throw new RuntimeException("该商品未被删除！");
        }
        //更改状态为未删除和为审核
        spu.setIsDelete("0");
        spu.setStatus("0");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    /**
     * 逻辑删除商品
     *
     * @param spuId
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void logicDelete(String spuId) {
        //查询商品信息
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        //可以逻辑删除的商品必须为下架的商品
        if (ISMARKET.equals(spu.getIsMarketable())) {
            throw new RuntimeException("商品为下架不能删除！");
        }
        //更改逻辑删除状态
        spu.setIsDelete("1");
        //更改为未审核
        spu.setStatus("0");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    /**
     * 上架商品
     *
     * @param spuId
     */
    @Override
    public void put(String spuId) {
        //查询商品
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        //判断商品是否已经被逻辑删除
        if (ISDELETE.equals(spu.getIsDelete())) {
            throw new RuntimeException("该商品已经被删除！");
        }
        String status = "1";
        //判断商品是否已被审核通过
        if (!status.equals(spu.getStatus())) {
            throw new RuntimeException("该商品未被审核通过！");
        }
        //上架商品
        spu.setIsMarketable("1");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    /**
     * 批量上架商品
     *
     * @param spuIds
     */
    @Override
    public Integer putMany(String[] spuIds) {
        //上架状态
        Spu spu = new Spu();
        spu.setIsMarketable("1");

        //设置能上上架商品的模板
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        //需要上架的商品id
        criteria.andIn("id", Arrays.asList(spuIds));
        //必须是已审核，并且未被逻辑删除,需要更改状态的商品
        criteria.andEqualTo("isDelete", "0");
        criteria.andEqualTo("status", "1");
        criteria.andEqualTo("isMarketable", "0");

        return spuMapper.updateByExampleSelective(spu, example);
    }

    /**
     * 下架商品
     *
     * @param spuId
     */
    @Override
    public void pull(String spuId) {
        //查询商品
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        //判断商品是否已经被逻辑删除
        if (ISDELETE.equals(spu.getIsDelete())) {
            throw new RuntimeException("该商品已经被删除！");
        }
        //下架商品
        spu.setIsMarketable("0");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    /**
     * 批量下架商品
     *
     * @param spuIds
     * @return
     */
    @Override
    public Integer pullMany(String[] spuIds) {
        //下架状态
        Spu spu = new Spu();
        spu.setIsMarketable("0");

        //设置需要下架的模板
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        //需要下架的商品id
        criteria.andIn("id", Arrays.asList(spuIds));
        //需要更改状态,未被删除的商品
        criteria.andEqualTo("isMarketable", "1");
        criteria.andEqualTo("isDelete", "0");

        return spuMapper.updateByExampleSelective(spu, example);
    }

    /**
     * 审核商品
     *
     * @param spuId
     */
    @Override
    public void audit(String spuId) {
        //查询商品
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        //判断商品是否已经被逻辑删除
        if (ISDELETE.equals(spu.getIsDelete())) {
            throw new RuntimeException("该商品已经被删除！");
        }
        //设置为已审核
        spu.setStatus("1");
        //设置为上架
        spu.setIsMarketable("1");
        //更新数据
        spuMapper.updateByPrimaryKeySelective(spu);

    }

    /**
     * 通过spuId查询商品信息
     *
     * @param spuId
     * @return
     */
    @Override
    public Goods findBySpuId(String spuId) {
        //查询spu信息
        Spu spu = spuMapper.selectByPrimaryKey(spuId);

        //通过spuId查询sku列表
        Sku sku = new Sku();
        sku.setSpuId(spuId);
        List<Sku> skuList = skuMapper.select(sku);

        //封装数据,返回
        Goods goods = new Goods();
        goods.setSpu(spu);
        goods.setSkuList(skuList);
        return goods;
    }

    /**
     * 添加商品信息
     *
     * @param goods
     */
    @Override
    public void addAndSave(Goods goods) {
        //获取添加商品spu信息
        Spu spu = goods.getSpu();
        //通过传入数据spu是否存在id判断是进行修改还是添加操作
        if (StringUtils.isEmpty(spu.getId())) {
            //执行添加操作
            //补全spu信息,生成随机id
            spu.setId("No" + idWorker.nextId());
            //添加spu
            spuMapper.insertSelective(spu);
        } else {
            //执行修改操作，修改spu信息
            spuMapper.updateByPrimaryKeySelective(spu);
            //删除下属所有sku信息
            Sku sku = new Sku();
            sku.setSpuId(spu.getId());
            skuMapper.delete(sku);
        }

        //获取添加商品sku信息
        List<Sku> skuList = goods.getSkuList();
        //补全sku信息
        for (Sku sku : skuList) {
            //id
            sku.setId("S" + idWorker.nextId());

            //名称使用spu,sku组装的形式
            String name = spu.getName();
            Map<String, String> specMap = JSON.parseObject(sku.getSpec(), Map.class);
            //拼接规格信息
            for (Map.Entry<String, String> entry : specMap.entrySet()) {
                name += " " + entry.getValue();
            }
            sku.setName(name);

            //创建时间，修改时间
            sku.setUpdateTime(new Date());
            sku.setCreateTime(new Date());

            //spuId
            sku.setSpuId(spu.getId());

            //分类id，分类名
            sku.setCategoryId(spu.getCategory3Id());
            //查询分类信息
            Category category = categoryMapper.selectByPrimaryKey(spu.getCategory3Id());
            sku.setCategoryName(category.getName());

            //品牌名
            Brand brand = brandMapper.selectByPrimaryKey(spu.getBrandId());
            sku.setBrandName(brand.getName());

            //执行添加操作
            skuMapper.insertSelective(sku);
        }
    }
}
