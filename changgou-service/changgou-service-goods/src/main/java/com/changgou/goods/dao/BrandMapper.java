package com.changgou.goods.dao;
import com.changgou.goods.pojo.Brand;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/****
 * @Author:itheima
 * @Description:Brand的Dao
 *****/
public interface BrandMapper extends Mapper<Brand> {
    /**
     * 通过分类Id查询品牌信息列表
     * @param cid
     * @return
     */
    @Select("SELECT b.* FROM tb_brand b, tb_category_brand cb WHERE cb.`brand_id` = b.`id` AND cb.`category_id`= #{cid}")
    List<Brand> findByCategoryId(Integer cid);
}
