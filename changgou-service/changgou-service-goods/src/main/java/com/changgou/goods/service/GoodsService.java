package com.changgou.goods.service;

import com.changgou.goods.pojo.Goods;

/**
 * @author Jun
 * @date 2021/1/10
 */
public interface GoodsService {

    /**
     * 添加商品信息
     *
     * @param goods
     */
    void addAndSave(Goods goods);

    /**
     * 通过spuId查询商品信息
     *
     * @param spuId
     * @return
     */
    Goods findBySpuId(String spuId);

    /**
     * 审核商品
     *
     * @param spuId
     */
    void audit(String spuId);

    /**
     * 下架商品
     *
     * @param spuId
     */
    void pull(String spuId);

    /**
     * 上架商品
     *
     * @param spuId
     */
    void put(String spuId);

    /**
     * 批量上架商品
     *
     * @param spuIds
     * @return
     */
    Integer putMany(String[] spuIds);

    /**
     * 批量下架商品
     *
     * @param spuIds
     * @return
     */
    Integer pullMany(String[] spuIds);

    /**
     * 逻辑删除商品
     *
     * @param spuId
     */
    void logicDelete(String spuId);

    /**
     * 恢复逻辑删除商品
     *
     * @param spuId
     */
    void restore(String spuId);

    /**
     * 物理删除商品
     * @param spuId
     */
    void delete(String spuId);

}
